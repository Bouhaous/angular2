package lu.maxime.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by DEMIJOLLAM on 20/09/2016.
 */
@RestController
@RequestMapping("/entity")
public class EntityController {

    @RequestMapping(value = "/public", method = RequestMethod.GET)
    public String getPublic() {
        return "public";
    }

    @RequestMapping(value = "/private", method = RequestMethod.GET)
    public String getPrivate() {
        return "private";
    }
}
