/**
 * Created by DEMIJOLLAM on 21/09/2016.
 */
module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jsSrc: ['src/main/resources/app/js'],
        jsDest: 'src/main/resources/META-INF/resources/app/js/<%= pkg.name %>.js',
        jsBanner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
        concat: {
            options: {
                // define a string to put between each file in the concatenated output
                separator: ';'
            },
            dist: {
                // the files to concatenate
                src: ['<%= jsSrc %>' + '**/*.js'],
                // the location of the resulting JS file
                dest: '[<%= jsDest %>]'
            }
        },
        uglify: {
            options: {
                // the banner is inserted at the top of the output
                banner: '<%= jsBanner %>]'
            },
            dist: {
                files: {
                    'src/main/resources/META-INF/resources/app/js/app.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        copy: {
            js: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/main/resources/app/js',
                        src: '**/*.js',
                        dest: 'src/main/resources/META-INF/resources/app/js/'
                    },{
                        expand: true,
                        cwd: 'node_modules/core-js',
                        src: '**/*.js',
                        dest: 'src/main/resources/META-INF/resources/lib/core-js'
                    },{
                        expand: true,
                        cwd: 'node_modules/zone.js',
                        src: '**/*.js',
                        dest: 'src/main/resources/META-INF/resources/lib/zone.js'
                    },{
                        expand: true,
                        cwd: 'node_modules/reflect-metadata',
                        src: '**/*.js',
                        dest: 'src/main/resources/META-INF/resources/lib/reflect-metadata'
                    },{
                        expand: true,
                        cwd: 'node_modules/systemjs',
                        src: '**/*.js',
                        dest: 'src/main/resources/META-INF/resources/lib/systemjs'
                    },{
                        expand: true,
                        cwd: 'node_modules/@angular',
                        src: '**/*.js',
                        dest: 'src/main/resources/META-INF/resources/lib/@angular'
                    },{
                        expand: true,
                        cwd: 'node_modules/rxjs',
                        src: '**/*.js',
                        dest: 'src/main/resources/META-INF/resources/lib/rxjs'
                    },{
                        expand: true,
                        cwd: 'node_modules/angular2-in-memory-web-api',
                        src: '**/*.js',
                        dest: 'src/main/resources/META-INF/resources/lib/angular2-in-memory-web-api'
                    },{
                        expand: true,
                        cwd: 'node_modules/bootstrap',
                        src: '**/*.js',
                        dest: 'src/main/resources/META-INF/resources/lib/bootstrap'
                    }
                ],
            },
            html: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/main/resources/app',
                        src: '**/*.html',
                        dest: 'src/main/resources/META-INF/resources/app'
                    }
                ],
            },
            style: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/main/resources/app/style',
                        src: '**/*.css',
                        dest: 'src/main/resources/META-INF/resources/app/style'
                    }
                ],
            }
        },
        targethtml: {
            dev: {
                files: {
                    'src/main/resources/META-INF/resources/index.html': 'src/main/resources/index.html'
                }
            },
            prod: {
                files: {
                    'src/main/resources/META-INF/resources/index.html': 'src/main/resources/index.html'
                }
            }
        },
        clean: ['src/main/resources/META-INF'],
        sass: {
            dist: {
                files: {
                    'src/main/resources/META-INF/resources/app/style/style.css': 'src/main/resources/app/style/style.scss',
                }
            }
        },
        ts: {
            default: {
                src: ['**/*.ts', '!node_modules/**'],
                dest: 'src/main/resources/META-INF/resources/app/js'
            },
            options: {
                target: "es5",
                module: "commonjs",
                moduleResolution: "node",
                sourceMap: true,
                emitDecoratorMetadata: true,
                experimentalDecorators: true,
                removeComments: false,
                noImplicitAny: false
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-targethtml');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks("grunt-ts");
    grunt.registerTask('dev', ['clean', 'ts', 'targethtml:dev', 'sass', 'copy:html', 'copy:js', 'copy:style']);
    grunt.registerTask('default', []);
    grunt.registerTask('prod', ['clean', 'targethtml:prod', 'sass', 'concat', 'uglify', 'copy:html', 'copy:style']);
    grunt.registerTask('style', ['sass']);
};